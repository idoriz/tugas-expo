import React, { Component } from 'react'
import { Image, StyleSheet, View } from 'react-native'

export default class App extends Component {
  render() {
    return (
        <View style = {styles.container}>
      <Image
        style={styles.image}
        source={{uri: 'http://www.reactnativeexpress.com/static/logo.png'}}
      />
        </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      },
  image: {
    width: 200,
    height: 200,
  },
})