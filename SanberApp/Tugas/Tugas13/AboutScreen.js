import React, { Component } from 'react'
import { Image, StyleSheet, View, TouchableOpacity, Text, TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'

export default class App extends Component {
  render() {
    return (
        <View style = {styles.container}>
            <TouchableOpacity>
                <View style = {styles.navBar}>
                    <Icon name = "md-arrow-round-back" size = {24}/>
                    <Text style = {{fontSize : 15, fontWeight:'bold'}}> BACK</Text>
                </View>
            </TouchableOpacity>
            <View style = {styles.body}>
            <View style = {styles.groups}>
                <Text style = {{fontSize : 24, fontWeight:'bold'}}>About Me</Text>
                <Image source = {require('./images/foto.png')} style = {{width: 130, height: 130, marginTop: 20}}/>
            </View>
            <View style = {styles.groups}>
                <View style = {styles.infoGroup}>
                    <Icon name = "logo-facebook" size = {33} color='blue'/>
                    <Text style = {styles.infoStyle}> IDRIS NURSALIM AL MUHAJIRI</Text>
                </View>
                <View style = {styles.infoGroup}>
                    <Icon name = "logo-twitter" size = {33}/>
                    <Text style = {styles.infoStyle}> @idoriz</Text>
                </View>
                <View style = {styles.infoGroup}>
                    <Icon name = "logo-instagram" size = {33}/>
                    <Text style = {styles.infoStyle}> @idoriz</Text>
                </View>
            </View>
            <View style = {styles.groups}>
                <Text style = {{fontSize : 24, fontWeight:'bold', margin:10}}>Portofolio :</Text>
                <View style = {styles.infoGroup}>
                    <Icon name = "logo-github" size = {33}/>
                    <Text style = {styles.infoStyle}> @idoriz</Text>
                </View>
                <View style = {styles.infoGroup}>
                    <Icon2 name = "gitlab" size = {33}/>
                    <Text style = {styles.infoStyle}> @idoriz</Text>
                </View>
                <View style = {styles.infoGroup}>
                    <Icon name = "logo-youtube" size = {33}/>
                    <Text style = {styles.infoStyle}> @idoriz</Text>
                </View>
            </View>
            </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
    navBar: {
        flexDirection:'row', 
        alignItems:'center',
        margin: 10
    },
    body:{
        flex : 1,
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    infoStyle:{
        fontSize : 20, 
        fontWeight:'bold'
    },
    infoGroup:{
        flexDirection:'row', 
        alignItems:'center',
        margin : 4
    },
    groups:{
        alignItems: 'center',
        justifyContent: 'center',
      }
     
})