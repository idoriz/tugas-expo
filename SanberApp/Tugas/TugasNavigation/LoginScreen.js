import React, { Component } from 'react'
import { Image, StyleSheet, View, TouchableOpacity, Text, TextInput } from 'react-native'

export default class App extends Component {
  render() {
    return (
        <View style = {styles.container}>
            <View style = {{alignItems: 'center', justifyContent: 'center', margin: 30}}>
                <Image source = {require('./images/logo.png')} style = {{width: 130, height: 140, marginTop: 20}}/>
                <Text style = {{fontSize : 20}}>Time to Show what you got</Text>
            </View>
            <View style = {styles.groups}>
                <Text style = {{fontSize : 13}}>Username/E-mail :</Text>
                <View style={styles.box}>
                    <TextInput
                    style={{ height: 40, textAlign: 'center'}}
                    />
                </View>
            </View>
            <View style = {styles.groups}>
            <Text style = {{fontSize : 13}}>Password :</Text>
                <View style={styles.box}>
                    <TextInput
                    style={{ height: 40, textAlign: 'center'}}
                    />
                </View>
            </View>
            <View style = {styles.groups}>
                <TouchableOpacity>
                    <View style={styles.button}>
                        <Text style = {styles.buttonText}>LOGIN</Text>
                    </View>
                </TouchableOpacity>
                <Text style = {{fontSize : 13}}>or</Text>
                <TouchableOpacity>
                    <View style={styles.button}>
                        <Text style = {styles.buttonText}>REGISTER</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <TouchableOpacity>
                <Text style = {{fontSize : 12, color: 'red', marginTop:30, marginBottom:20}}>Developer's Info</Text>
            </TouchableOpacity>
        </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
      },
      box: {
        width: 233,
        height: 43,
        backgroundColor: '#e5e5e5',
        justifyContent:'center',
        borderRadius : 5,
        margin : 6
      },
      button: {
        width: 85,
        height: 38,
        backgroundColor: 'blue',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius : 10,
        margin: 8
      },
      buttonText: {
        fontSize : 13, 
        color:'white',
        fontWeight: 'bold',
      },
      groups:{
        alignItems: 'center',
        justifyContent: 'center',
      }
})