import React from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'

export default class SkillScreen extends React.Component {
    render(){
        return(
            <View style = {styles.container}>
                <TouchableOpacity style={styles.menubutton}>
                    <Icon2 name='menu' size={25}/>
                </TouchableOpacity>

                <Text style = {styles.judul}>User Profile</Text>

                <View style={styles.profil}>
                    <View style={styles.dummyfoto}/>
                    <Text style={styles.subjudul}>Ilyas Nur Fatah</Text>
                </View>

                <Text style = {styles.judul}>Programming Skills</Text>
                <ScrollView>
                    <View style = {styles.body}>  
                        <Text style = {styles.subjudul}>Languages</Text>
                        <View style={styles.subjudulcont}>
                            <View style = {styles.itemskill}>
                                <Icon2 name = "language-cpp" size = {25}/>
                                <View style={styles.barcont}>
                                    <Text>C++</Text>
                                    <View style = {styles.bar}>
                                        <View style = {styles.skillbar}>
                                            <View style = {styles.levelskillAdv} />
                                        </View>
                                        <Text>  Advanced</Text>
                                    </View>
                                </View>
                            </View>
                            <View style = {styles.itemskill}>
                                <Icon2 name = "language-javascript" size = {25}/>
                                <View style={styles.barcont}>
                                    <Text>Javascript</Text>
                                    <View style = {styles.bar}>
                                        <View style = {styles.skillbar}>
                                            <View style = {styles.levelskillBeg} />
                                        </View>
                                        <Text>  Beginner</Text>
                                    </View>
                                </View>
                            </View>
                        </View>

                        <Text style = {styles.subjudul}>Framework</Text>
                        <View style={styles.subjudulcont}>
                            <View style = {styles.itemskill}>
                                <Icon2 name = "react" size = {25}/>
                                <View style={styles.barcont}>
                                    <Text>React Native</Text>
                                    <View style = {styles.bar}>
                                        <View style = {styles.skillbar}>
                                            <View style = {styles.levelskillInt} />
                                        </View>
                                        <Text>  Intermediate</Text>
                                    </View>
                                </View>

                            </View>
                        </View>

                        <Text style = {styles.subjudul}>Technologies</Text>
                        <View style={styles.subjudulcont}>
                            <View style = {styles.itemskill}>
                                <Icon2 name = "github-circle" size = {25}/>
                                <View style={styles.barcont}>
                                    <Text>Github</Text>
                                    <View style = {styles.bar}>
                                        <View style = {styles.skillbar}>
                                            <View style = {styles.levelskillAdv} />
                                        </View>
                                        <Text>  Advanced</Text>
                                    </View>
                                </View>
                            </View>
                            <View style = {styles.itemskill}>
                                <Icon2 name = "gitlab" size = {25}/>
                                <View style={styles.barcont}>
                                    <Text>Gitlab</Text>
                                    <View style = {styles.bar}>
                                        <View style = {styles.skillbar}>
                                            <View style = {styles.levelskillBeg} />
                                        </View>
                                        <Text>  Beginner</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>                
                </ScrollView>
                <View style = {styles.botBar}>
                    <TouchableOpacity>
                    <Icon name = 'ios-home' size = {50}/>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex : 1,
        alignItems : 'center',
        justifyContent: 'center'
    },
    menubutton:{
        position: 'absolute',
        left: 20,
        top: 20,
    },
    judul:{
        fontSize : 24,
        borderBottomWidth : 2,
        margin : 10
    },
    subjudulcont:{
        justifyContent : 'flex-start'
    },
    subjudul:{
        fontSize : 20,
        padding : 10
    },
    profil:{
        flexDirection: 'row',
        alignItems: 'center'
    },
    dummyfoto:{
        width: 42,
        height: 42,
        backgroundColor: '#e5e5e5',
        borderRadius: 21,
    },
    body:{
        flex : 1,
        alignItems : 'center',
    },
    itemskill:{
        flexDirection : 'row',
        alignItems: 'center',
    },
    barcont:{
        padding : 10
    },
    bar:{
        flexDirection : 'row',
        alignItems : 'baseline',
    },
    skillbar:{
        height: 9,
        width: 150,
        backgroundColor: '#e5e5e5'
    },
    levelskillBeg:{
        height: 9,
        width: 50,
        backgroundColor: 'orange'
    },
    levelskillInt:{
        height: 9,
        width: 100,
        backgroundColor: 'orange'
    },
    levelskillAdv:{
        height: 9,
        width: 130,
        backgroundColor: 'orange'
    },
    botBar:{
        alignItems : 'center',
        backgroundColor : '#e5e5e5',
        paddingTop: 20,
        height : 90,
        width : 380
    },
})